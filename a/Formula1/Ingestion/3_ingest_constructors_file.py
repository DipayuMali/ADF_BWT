# Databricks notebook source
dbutils.widgets.text("p_data_source","")
v_data_source = dbutils.widgets.get("p_data_source")

# COMMAND ----------

dbutils.widgets.text("p_file_date","")
v_file_date = dbutils.widgets.get("p_file_date")

# COMMAND ----------

# MAGIC %run "../includes/configuration"

# COMMAND ----------

# MAGIC %run "../includes/common_functions"

# COMMAND ----------

# MAGIC %md
# MAGIC ## Ingest costructors.json file

# COMMAND ----------

# MAGIC %md
# MAGIC ### step 1 - read json file using spark datafrsme reader

# COMMAND ----------

costructors_schema1 = "constructorId INT, constructorRef STRING, name STRING, nationality STRING, url STRING"

# COMMAND ----------

costructor_df = spark.read.schema(costructors_schema1).json(f"{raw_folder_path}/{v_file_date}/constructors.json")

# COMMAND ----------

costructor_df.printSchema()

# COMMAND ----------

display(costructor_df)

# COMMAND ----------

display(costructor_df.describe)

# COMMAND ----------

# MAGIC %md
# MAGIC ### step 2 - drop unwanted column

# COMMAND ----------

constructor_dropped_df = costructor_df.drop('url')

# COMMAND ----------

# MAGIC %md
# MAGIC ### step 3 - renamed column and ingest date

# COMMAND ----------

from pyspark.sql.functions  import current_timestamp,lit

# COMMAND ----------

constructor_final_df = constructor_dropped_df.withColumnRenamed('constructorId',"constructor_Id").withColumnRenamed('constructorRef','constructor_ref').withColumn('ingestion_date',current_timestamp()).withColumn("data_source",lit(v_data_source)).withColumn("file_date",lit(v_file_date))

# COMMAND ----------

display(constructor_final_df)

# COMMAND ----------

# MAGIC %md
# MAGIC ### step 3 - write file in parquet

# COMMAND ----------

constructor_final_df.write.mode("overwrite").format("delta").saveAsTable("f1_processed.constructors")

# COMMAND ----------

# MAGIC %fs
# MAGIC ls /mnt/formula1dl11/processed/constructors

# COMMAND ----------

# MAGIC %sql
# MAGIC select * from f1_processed.constructors

# COMMAND ----------

dbutils.notebook.exit("success")

# COMMAND ----------


