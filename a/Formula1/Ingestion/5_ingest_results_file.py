# Databricks notebook source
dbutils.widgets.text("p_data_source","")
v_data_source = dbutils.widgets.get("p_data_source")

# COMMAND ----------

dbutils.widgets.text("p_file_date","2021-03-28")
v_file_date = dbutils.widgets.get("p_file_date")

# COMMAND ----------

# MAGIC %run "../includes/configuration"

# COMMAND ----------

# MAGIC %run "../includes/common_functions"

# COMMAND ----------

# MAGIC %md
# MAGIC ## Ingest result.json file

# COMMAND ----------

# MAGIC %md
# MAGIC ### step 1 - read json file using spark datafrsme reader API

# COMMAND ----------

from pyspark.sql.types import StructField,StructType,StringType,StringType,IntegerType,DataType,FloatType

# COMMAND ----------

# MAGIC %md
# MAGIC ##step2 -renamed column and add new column

# COMMAND ----------

results_schema = StructType([StructField("resultId",IntegerType(),False),
                             StructField("raceId",IntegerType(),True),
                             StructField("driverId",IntegerType(),True),
                             StructField("constructorId",IntegerType(),True),
                             StructField("number",IntegerType(),True),
                             StructField("grid",IntegerType(),True),
                             StructField("position",IntegerType(),True),
                             StructField("positionText",StringType(),True),
                             StructField("positionOrder",IntegerType(),True),
                             StructField("points",FloatType(),True),
                             StructField("laps",IntegerType(),True),
                             StructField("time",StringType(),True),
                             StructField("milliseconds",IntegerType(),True),
                             StructField("fastestLap",IntegerType(),True),
                             StructField("rank",IntegerType(),True),
                             StructField("fastestLapTime",StringType(),True),
                             StructField("fastestLapSpeed",FloatType(),True),
                             StructField("statusId",IntegerType(),True)])

        

# COMMAND ----------

results_df = spark.read.schema(results_schema).json(f"{raw_folder_path}/{v_file_date}/results.json")

# COMMAND ----------

from pyspark.sql.functions import current_timestamp,lit

# COMMAND ----------

results_with_column_df = results_df.withColumnRenamed("resultId","result_id").withColumnRenamed("raceId","race_id").withColumnRenamed("driverId","driver_id").withColumnRenamed("constructorId","constructor_id").withColumnRenamed("positionText","position_text").withColumnRenamed("positionOrder","position_order").withColumnRenamed("fastestLap","fastest_lap").withColumnRenamed("fastestLapTime","fastest_lap_time").withColumnRenamed("fastestLapSpeed","fastest_lap_speed").withColumn("ingestion_date",current_timestamp()).withColumn("data_source",lit(v_data_source)).withColumn("file_date",lit(v_file_date))

# COMMAND ----------

# MAGIC %md
# MAGIC ### step 3 - drop column

# COMMAND ----------

from pyspark.sql.functions import col

# COMMAND ----------

results_final_df = results_with_column_df.drop(col('statusId'))

# COMMAND ----------

results_deduped_df = results_final_df.dropDuplicates(['race_id','driver_id'])

# COMMAND ----------

# MAGIC %md
# MAGIC ##step 4 - write data in parquet

# COMMAND ----------

# MAGIC %md
# MAGIC ## method 1

# COMMAND ----------

# for race_id_list in results_final_df.select("race_id").distinct().collect():
#     if (spark._jsparkSession.catalog().tableExists("f1_processed.results")):
#         spark.sql(f"ALTER TABLE f1_processed.results DROP IF EXISTS PARTITION (race_Id = {race_id_list.race_id})")

# COMMAND ----------

# results_final_df.write.mode("append").partitionBy("race_Id").format("parquet").saveAsTable("f1_processed.results")

# COMMAND ----------

# overwrite_partition(results_final_df,"f1_processed","results","race_id")

# COMMAND ----------

merge_condition = "tgt.result_id = src.result_id AND tgt.race_id = src.race_id"

merge_delta_data(results_deduped_df,"f1_processed","results",processed_folder_path,merge_condition,"race_id")


# COMMAND ----------

dbutils.notebook.exit("success")

# COMMAND ----------

# MAGIC %sql
# MAGIC select race_id from f1_processed.results
# MAGIC order by race_id desc

# COMMAND ----------

# MAGIC %sql 
# MAGIC select count(*),race_id,driver_id from f1_processed.results
# MAGIC group by race_id,driver_id
# MAGIC order by race_id,driver_id desc

# COMMAND ----------

# MAGIC %sql
# MAGIC drop table f1_processed.results

# COMMAND ----------


