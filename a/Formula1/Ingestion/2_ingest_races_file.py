# Databricks notebook source
dbutils.widgets.text("p_data_source","")
v_data_source = dbutils.widgets.get("p_data_source")

# COMMAND ----------

dbutils.widgets.text("p_file_date","")
v_file_date = dbutils.widgets.get("p_file_date")

# COMMAND ----------

# MAGIC %run "../includes/configuration"

# COMMAND ----------

# MAGIC %run "../includes/common_functions"

# COMMAND ----------

# MAGIC %md
# MAGIC ## Ingest races.csv file
# MAGIC

# COMMAND ----------

# MAGIC %md
# MAGIC ### step 1 - read csv file using spark datafrsme reader

# COMMAND ----------

from pyspark.sql.types import StructType,StructField,IntegerType,StringType,DateType

# COMMAND ----------

races_schema = StructType([StructField("raceid",IntegerType(),False),
                           StructField("year",IntegerType(),True),
                           StructField("round",IntegerType(),True),
                           StructField("circuitid",IntegerType(),True),
                           StructField("name",StringType(),True),
                           StructField("date",DateType(),True),
                           StructField("time",StringType(),True),
                           StructField("url",StringType(),True)])
                           


# COMMAND ----------

races_df = spark.read.option("header",True).schema(races_schema).csv(f"{raw_folder_path}/{v_file_date}/races.csv")

# COMMAND ----------

display(races_df)

# COMMAND ----------

# MAGIC %md
# MAGIC ### step 2 - add ingestion date and race_timnestamp to the dataframe

# COMMAND ----------

from pyspark.sql.functions import current_timestamp,to_timestamp,col,lit,concat

# COMMAND ----------

races_with_timestamp_df = races_df.withColumn("ingestion_date",current_timestamp()).withColumn("race_timestamp",to_timestamp(concat(col("date"),lit(' '),col('time')),'yyyy-MM-dd HH:mm:ss')).withColumn("data_source",lit(v_data_source)).withColumn("file_date",lit(v_file_date))

# COMMAND ----------

display(races_with_timestamp_df)

# COMMAND ----------

from pyspark.sql.functions import current_timestamp,to_timestamp,col,lit,concat

# COMMAND ----------

# MAGIC %md
# MAGIC ### step 3 - select required column and renamed

# COMMAND ----------

races_selected_df = races_with_timestamp_df.select(col('raceid').alias('race_id'),col('year').alias('race_year'),col('round'),col('circuitid').alias('circuit_id'),col('name'),col('ingestion_date'),col('race_timestamp'))

# COMMAND ----------

display(races_selected_df)

# COMMAND ----------

# MAGIC %md
# MAGIC ### step 4 -write file in parquet

# COMMAND ----------

races_selected_df.write.mode("overwrite").partitionBy('race_year').format("delta").saveAsTable("f1_processed.races")

# COMMAND ----------

# MAGIC %fs
# MAGIC ls /mnt/formula1dl11/processed/races

# COMMAND ----------

# MAGIC %sql
# MAGIC select * from f1_processed.races

# COMMAND ----------

dbutils.notebook.exit("success")

# COMMAND ----------


