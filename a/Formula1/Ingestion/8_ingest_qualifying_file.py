# Databricks notebook source
dbutils.widgets.text("p_data_source","")
v_data_source = dbutils.widgets.get("p_data_source")

# COMMAND ----------

dbutils.widgets.text("p_file_date","2021-03-28")
v_file_date = dbutils.widgets.get("p_file_date")

# COMMAND ----------

# MAGIC %run "../includes/configuration"

# COMMAND ----------

# MAGIC %run "../includes/common_functions"

# COMMAND ----------

# MAGIC %md
# MAGIC ##Ingest qualifying json files

# COMMAND ----------

# MAGIC %md
# MAGIC ### step 1 - read json file using spark datafrsme reader API

# COMMAND ----------

from pyspark.sql.types import StructType,StructField,StringType,IntegerType,DateType

# COMMAND ----------

qualifying_schemas = StructType([StructField("qualifyId",IntegerType(),False),
                                 StructField("raceId",IntegerType(),True),
                                 StructField("driverId",IntegerType(),True),
                                 StructField("constructorId",IntegerType(),True),
                                 StructField("number",IntegerType(),True),
                                 StructField("position",IntegerType(),True),
                                 StructField("q1",StringType(),True),
                                 StructField("q2",StringType(),True),
                                 StructField("q3",StringType(),True)
                                 ])

# COMMAND ----------

qualifying_df = spark.read.schema(qualifying_schemas).option("multiLine",True).json(f"{raw_folder_path}/{v_file_date}/qualifying")

# COMMAND ----------

# MAGIC %md
# MAGIC ## renamed columns add column

# COMMAND ----------

from pyspark.sql.functions import current_timestamp,lit

# COMMAND ----------

final_df = qualifying_df.withColumnRenamed("qualifyId","qualify_id").withColumnRenamed('driverId',"driver_id").withColumnRenamed('raceId',"race_id").withColumnRenamed("constructorId","constructor_id").withColumn("ingestion_date",current_timestamp()).withColumn("data_source",lit(v_data_source)).withColumn("file_date",lit(v_file_date))

# COMMAND ----------

# final_df.write.mode("overwrite").format("parquet").saveAsTable("f1_processed.qualifying")
# overwrite_partition(final_df,"f1_processed","qualifying","race_id")


# COMMAND ----------

merge_condition = "tgt.qualify_id = src.qualify_id"

merge_delta_data(final_df,"f1_processed","qualifying",processed_folder_path,merge_condition,"race_id")

# COMMAND ----------

# MAGIC %sql
# MAGIC select * from f1_processed.qualifying

# COMMAND ----------

dbutils.notebook.exit("success")

# COMMAND ----------

# MAGIC %sql
# MAGIC select race_Id, count(1) from f1_processed.qualifying
# MAGIC group by race_Id
# MAGIC order by race_Id desc

# COMMAND ----------

# MAGIC %sql
# MAGIC drop table f1_processed.qualifying
