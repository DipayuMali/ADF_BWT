# Databricks notebook source
dbutils.widgets.text("p_data_source","")
v_data_source = dbutils.widgets.get("p_data_source")

# COMMAND ----------

dbutils.widgets.text("p_file_date","")
v_file_date = dbutils.widgets.get("p_file_date")

# COMMAND ----------

v_file_date

# COMMAND ----------

# MAGIC %run "../includes/configuration"

# COMMAND ----------

# MAGIC %run "../includes/common_functions"

# COMMAND ----------

raw_folder_path

# COMMAND ----------

# MAGIC %md
# MAGIC ## Ingest circuits.csv file
# MAGIC

# COMMAND ----------

# MAGIC %md
# MAGIC ### step 1 - read csv file using spark datafrsme reader

# COMMAND ----------

from pyspark.sql.types import StructType,StructField,IntegerType,StringType,DoubleType

# COMMAND ----------

circuits_schema = StructType(fields=[StructField('circuitid',IntegerType(),False),
                                      StructField('circuitRef',StringType(),True),
                                      StructField('name',StringType(),True),
                                      StructField('location',StringType(),True),
                                      StructField('country',StringType(),True),
                                      StructField('lat',DoubleType(),True),
                                      StructField('lng',DoubleType(),True),
                                      StructField('alt',IntegerType(),True),
                                      StructField('url',StringType(),True)])

# COMMAND ----------

circuits_df = spark.read.option("header",True).schema(circuits_schema).csv(f"{raw_folder_path}/{v_file_date}/circuits.csv")

# COMMAND ----------

type(circuits_df)

# COMMAND ----------

circuits_df.show()

# COMMAND ----------

display(circuits_df)

# COMMAND ----------

circuits_df.printSchema()

# COMMAND ----------

from pyspark.sql.functions import col

# COMMAND ----------

# MAGIC %md
# MAGIC ### step 2 - drop column or select columns which are required

# COMMAND ----------

# MAGIC %md
# MAGIC ##circuits_selected_df = circuits_df.select("circuitid","circuitRef",col("name"),"location",circuits_df["country"],circuits_df.lat,"lng","alt")

# COMMAND ----------

circuits_selected_df = circuits_df.select(col("circuitid"),col("circuitRef"),col("name"),col("location"),col("country"),col("lat"),col("lng"),col("alt"))

# COMMAND ----------

display(circuits_selected_df)

# COMMAND ----------

# MAGIC %md
# MAGIC ## step 3 Renaming columns

# COMMAND ----------

from pyspark.sql.functions import lit
circuits_renamed_df = circuits_selected_df.withColumnRenamed("circuitid","circuit_id").withColumnRenamed("circuitRef","circuit_ref").withColumnRenamed("lat","latitude").withColumnRenamed("lng","longitude").withColumnRenamed("alt","altitude").withColumn("data_source",lit(v_data_source)).withColumn("file_date",lit(v_file_date))

# COMMAND ----------

display(circuits_renamed_df)

# COMMAND ----------

# MAGIC %md
# MAGIC ### step 4 - add ingestion date to dataframe

# COMMAND ----------

from pyspark.sql.functions import current_timestamp,lit

# COMMAND ----------

circuits_final_df = add_ingestion_date(circuits_renamed_df)

# COMMAND ----------

display(circuits_final_df)

# COMMAND ----------

# MAGIC %md
# MAGIC ### step 4 - write file in location

# COMMAND ----------

circuits_final_df.write.mode("overwrite").format("delta").saveAsTable("f1_processed.circuits")

# COMMAND ----------

# MAGIC %fs
# MAGIC ls /mnt/formula1dl11/processed/circuits

# COMMAND ----------

# MAGIC %sql
# MAGIC select * from f1_processed.circuits

# COMMAND ----------

dbutils.notebook.exit("success")
