-- Databricks notebook source
create database if not exists f1_raw;

-- COMMAND ----------

-- MAGIC %md
-- MAGIC ## create circuit tables

-- COMMAND ----------

drop table if exists f1_raw.circuits;
create table if not exists f1_raw.circuits(circuitid int,
circuitRef string,
name string,
location string, 
country string,
lat double,
lng double,
alt int,
url string) 
using csv options(path "/mnt/formula1dl11/raw/circuits.csv",header true)

-- COMMAND ----------

select * from f1_raw.circuits

-- COMMAND ----------

-- MAGIC %md 
-- MAGIC ## create races table

-- COMMAND ----------

drop table if exists f1_raw.races;
create table if not exists f1_raw.races(raceid int,
year int,
round int,
circuitid int, 
name string,
date date,
time string,
url string) 
using csv options(path "/mnt/formula1dl11/raw/races.csv",header true)

-- COMMAND ----------

select * from f1_raw.races

-- COMMAND ----------

-- MAGIC %md
-- MAGIC ## create costructors table simple structure

-- COMMAND ----------

drop table if exists f1_raw.constructors;
create table if not exists f1_raw.constructors
(constructorId int,
constructorRef string,
name string,
nationality string, 
url string) 
using json options(path "/mnt/formula1dl11/raw/constructors.json")

-- COMMAND ----------

select * from f1_raw.constructors

-- COMMAND ----------

-- MAGIC %md
-- MAGIC ## create drivers table complex structure

-- COMMAND ----------

drop table if exists f1_raw.drivers;
create table if not exists f1_raw.drivers
(driverId int,
driverRef string,
number int,
code string, 
name struct<forename: string,surname: string>,
dob date,
nationality string,
url string) 
using json options(path "/mnt/formula1dl11/raw/drivers.json")

-- COMMAND ----------

select * from f1_raw.drivers;

-- COMMAND ----------

-- MAGIC %md
-- MAGIC ## results table

-- COMMAND ----------

drop table if exists f1_raw.results;
create table if not exists f1_raw.results(
resultId int,
raceId int,
driverId int,
constructorId int,
number int,
grid int,
position int,
positionText string,
positionOrder int,
points int,
laps int,
time string,
milliseconds int,
fastestLap int,
rank int,
fastestLapTime string,
fastestLapSpeed float,
statusId string)
using json options(path "/mnt/formula1dl11/raw/results.json")

-- COMMAND ----------

select * from f1_raw.results

-- COMMAND ----------

-- MAGIC %md
-- MAGIC ## pit stop table

-- COMMAND ----------

drop table if exists f1_raw.pit_stops;
create table if not exists f1_raw.pit_stops(
raceId Int,
driverId Int,
stop String,
lap Int,
time String,
duration String,
milliseconds Int)
using json options(path "/mnt/formula1dl11/raw/pit_stops.json",multiLine true)


-- COMMAND ----------

select * from f1_raw.pit_stops

-- COMMAND ----------

-- MAGIC %md
-- MAGIC ## lap time file

-- COMMAND ----------

drop table if exists f1_raw.lap_times;
create table if not exists f1_raw.lap_times(
raceId Int,
driverId int,
lap Int,
position Int,
time String,
milliseconds Int)
using csv 
options(path "/mnt/formula1dl11/raw/lap_times")


-- COMMAND ----------

select count(*) from f1_raw.lap_times

-- COMMAND ----------

select * from f1_raw.lap_times

-- COMMAND ----------

-- MAGIC %md
-- MAGIC ## qualifying file

-- COMMAND ----------

drop table if exists f1_raw.qualifying;
create table if not exists f1_raw.qualifying(
qualifyId Int,
raceId Int,
driverId Int,
constructorId Int,
number Int,
position Int,
q1 String,
q2 String,
q3 String
)
using json options(path "/mnt/formula1dl11/raw/qualifying",multiLine true)


-- COMMAND ----------

select * from f1_raw.qualifying

-- COMMAND ----------


