# Databricks notebook source
# MAGIC %md
# MAGIC ###Access Azure data lake using _cluster_scoped_credentials
# MAGIC 1. set the spark config fs.azure.account.key in the spark cluster
# MAGIC 2. lists files from demo container    3.read data from circuit.csv

# COMMAND ----------

display(dbutils.fs.ls("abfss://demo@formula1dl11.dfs.core.windows.net"))

# COMMAND ----------

display(spark.read.csv("abfss://demo@formula1dl11.dfs.core.windows.net/002 circuits.csv"))

# COMMAND ----------


