# Databricks notebook source
# MAGIC %md
# MAGIC ###Access Azure data lake using access keys
# MAGIC 1. set the spark config for sas token
# MAGIC 2. lists files from demo container    3.read data from circuit.csv

# COMMAND ----------

formula1dl11_demo_sas_token = dbutils.secrets.get(scope='formula1-scope',key='formula1dl11-demo-sas-token')

# COMMAND ----------

spark.conf.set("fs.azure.account.auth.type.formula1dl11.dfs.core.windows.net","SAS")
spark.conf.set("fs.azure.sas.token.provider.type.formula1dl11.dfs.core.windows.net","org.apache.hadoop.fs.azurebfs.sas.FixedSASTokenProvider")
spark.conf.set("fs.azure.sas.fixed.token.formula1dl11.dfs.core.windows.net",formula1dl11_demo_sas_token)

# COMMAND ----------

display(dbutils.fs.ls("abfss://demo@formula1dl11.dfs.core.windows.net"))

# COMMAND ----------

display(spark.read.csv("abfss://demo@formula1dl11.dfs.core.windows.net/002 circuits.csv"))

# COMMAND ----------


