# Databricks notebook source
# MAGIC %md
# MAGIC ###Access Azure data lake using service_principal
# MAGIC 1. register azure AD application/service principal
# MAGIC 2. generate secret/password for the application   3.set the spark config for the app/client ID,directory/tenant id and secret  4.Assing role 'storage blob contributor' to the data lake

# COMMAND ----------

client_id = dbutils.secrets.get(scope= 'formula1-scope',key='formula1-app-client-id')
tenant_id = dbutils.secrets.get(scope= 'formula1-scope',key='formula1-app-tenant-id')
client_secret = dbutils.secrets.get(scope= 'formula1-scope',key='formula1-app-client-secret')

# COMMAND ----------

spark.conf.set("fs.azure.account.auth.type.formula1dl11.dfs.core.windows.net", "OAuth")
spark.conf.set("fs.azure.account.oauth.provider.type.formula1dl11.dfs.core.windows.net", "org.apache.hadoop.fs.azurebfs.oauth2.ClientCredsTokenProvider")
spark.conf.set("fs.azure.account.oauth2.client.id.formula1dl11.dfs.core.windows.net", client_id)
spark.conf.set("fs.azure.account.oauth2.client.secret.formula1dl11.dfs.core.windows.net", client_secret)
spark.conf.set("fs.azure.account.oauth2.client.endpoint.formula1dl11.dfs.core.windows.net", f"https://login.microsoftonline.com/{tenant_id}/oauth2/token")

# COMMAND ----------

display(dbutils.fs.ls("abfss://demo@formula1dl11.dfs.core.windows.net"))

# COMMAND ----------

display(spark.read.csv("abfss://demo@formula1dl11.dfs.core.windows.net/002 circuits.csv"))

# COMMAND ----------


