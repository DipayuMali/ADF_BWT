# Databricks notebook source
# MAGIC %md
# MAGIC ###Access Azure data lake using access keys
# MAGIC 1. set the spark config fs.azure.account.key
# MAGIC 2. lists files from demo container    3.read data from circuit.csv

# COMMAND ----------

formula1dl11_account_key = dbutils.secrets.get(scope='formula1-scope',key='formula1dl11-account-key')

# COMMAND ----------

spark.conf.set (
    "fs.azure.account.key.formula1dl11.dfs.core.windows.net",
    formula1dl11_account_key
)

# COMMAND ----------

display(dbutils.fs.ls("abfss://demo@formula1dl11.dfs.core.windows.net"))

# COMMAND ----------

display(spark.read.csv("abfss://demo@formula1dl11.dfs.core.windows.net/002 circuits.csv"))

# COMMAND ----------


