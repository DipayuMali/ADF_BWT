# Databricks notebook source
# MAGIC  %md
# MAGIC  ### explore DBFS root  
# MAGIC  1.List all the folders in DBFS root 2.Interact with DBFS File browser 3.Upload file to DBFS root
# MAGIC

# COMMAND ----------

display(dbutils.fs.ls('/'))

# COMMAND ----------

display(dbutils.fs.ls('/FileStore/'))

# COMMAND ----------

display(spark.read.csv('dbfs:/FileStore/002_circuits.csv'))

# COMMAND ----------


