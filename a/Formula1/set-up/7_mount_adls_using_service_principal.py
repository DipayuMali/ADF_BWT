# Databricks notebook source
# MAGIC %md
# MAGIC ###Access Azure data lake using service_principal
# MAGIC 1. get client_id,tenant_id_and client_secret from the key vault
# MAGIC 2. set spark config with app/client id.directory/Tenant id & secret 
# MAGIC 3. call file system utility  mount to mount the storage   4.explore other file system utilities related to mount(list all mount,unmount)

# COMMAND ----------

client_id = dbutils.secrets.get(scope= 'formula1-scope',key='formula1-app-client-id')
tenant_id = dbutils.secrets.get(scope= 'formula1-scope',key='formula1-app-tenant-id')
client_secret = dbutils.secrets.get(scope= 'formula1-scope',key='formula1-app-client-secret')

# COMMAND ----------

configs = {"fs.azure.account.auth.type": "OAuth",
          "fs.azure.account.oauth.provider.type": "org.apache.hadoop.fs.azurebfs.oauth2.ClientCredsTokenProvider",
          "fs.azure.account.oauth2.client.id": client_id,
          "fs.azure.account.oauth2.client.secret": client_secret,
          "fs.azure.account.oauth2.client.endpoint": f"https://login.microsoftonline.com/{tenant_id}/oauth2/token"}


# COMMAND ----------

dbutils.fs.mount(
  source = "abfss://demo@formula1dl11.dfs.core.windows.net/",
  mount_point = "/mnt/formula1dl11/demo",
  extra_configs = configs)

# COMMAND ----------

display(dbutils.fs.ls("/mnt/formula1dl11/demo"))

# COMMAND ----------

display(spark.read.csv("/mnt/formula1dl11/demo/002 circuits.csv"))

# COMMAND ----------

display(dbutils.fs.mounts())

# COMMAND ----------

dbutils.fs.unmount('/mnt/formula1dl11/demo')

# COMMAND ----------


