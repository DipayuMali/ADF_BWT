# Databricks notebook source
# MAGIC %run "../includes/configuration"

# COMMAND ----------

race_df = spark.read.parquet(f"{processed_folder_path}/races")
results_df = spark.read.parquet(f"{processed_folder_path}/results")
driver_df = spark.read.parquet(f"{processed_folder_path}/drivers")
constructor_df = spark.read.parquet(f"{processed_folder_path}/constructors")
qualifying_df = spark.read.parquet(f"{processed_folder_path}/qualifying")


# COMMAND ----------

from pyspark.sql.functions import concat,lit,col

# COMMAND ----------

constructor_df = constructor_df.select("constructor_id",col("name").alias("team"))
final_result_df = results_df.select("race_id","driver_id","points")
race_df1 = race_df.select("race_id","race_year")

# COMMAND ----------

result_driver = final_result_df.join(driver_df,"driver_id","left").join(race_df1,"race_id","left")

# COMMAND ----------

from pyspark.sql.functions import *
from pyspark.sql.window import *
windospec = Window.partitionBy('race_year').orderBy(col('points').desc())
a = result_driver.withColumn('r', dense_rank().over(windospec)).filter(col("r")==1).drop("r")
a.select("name","race_year","points").distinct().display()

# COMMAND ----------

constructor_df1 = constructor_df.select("constructor_id","team")
final_result_df = results_df.select("race_id","constructor_id","points")
race_df1 = race_df.select("race_id","race_year")

# COMMAND ----------



# COMMAND ----------

result_team = final_result_df.join(constructor_df1,"constructor_id","left").join(race_df1,"race_id","left")

# COMMAND ----------

windospec1 = Window.partitionBy('race_year').orderBy(col('points').desc())
b = result_team.withColumn('r', dense_rank().over(windospec1)).filter(col("r")==1).drop("r")
b.select("team","race_year","points").distinct().display()

# COMMAND ----------

constructor_df1 = constructor_df.select("constructor_id","team")
driver_df1 = driver_df.select("driver_id","number","name")
qualifying_df1 = qualifying_df.select("constructor_id","driver_id",col("q1").alias("qualifyin1"))

# COMMAND ----------

qualifying_df2  = qualifying_df1.withColumn("qualifyin1",regexp_replace("qualifyin1","\\\\N","10000"))

# COMMAND ----------

qualifying_df2 = qualifying_df2.filter(col("qualifyin1")!='').filter(col("qualifyin1")!='\\\\N')

# COMMAND ----------

result_qual = qualifying_df2.join(constructor_df1,"constructor_id","left").join(driver_df1,"driver_id","left")

# COMMAND ----------

windospec2 = Window.orderBy(col('qualifyin1'))
b = result_qual.withColumn('r', dense_rank().over(windospec2)).filter(col("r")==1).drop("r")
b.select("name","team","qualifyin1").distinct().display()

# COMMAND ----------


