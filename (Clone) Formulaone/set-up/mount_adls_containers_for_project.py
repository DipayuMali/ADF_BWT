# Databricks notebook source
# MAGIC %md
# MAGIC ### Mount Azure data lake containers for the project

# COMMAND ----------

def mount_adls(storage_account_name,container_name):
    # get secrets from the key vault
    client_id = 'c7aededd-d4d5-4b39-9648-888473f51e3b'
    tenant_id = '45363a3d-e8d3-435e-b76d-3ff2c688441e'
    client_secret = '1BH8Q~72gxY6ZgYS2dj~SYbtJyvjYAEmnFgSFaqO'


    # set spark configuration
    configs = {"fs.azure.account.auth.type": "OAuth",
          "fs.azure.account.oauth.provider.type": "org.apache.hadoop.fs.azurebfs.oauth2.ClientCredsTokenProvider",
          "fs.azure.account.oauth2.client.id": client_id,
          "fs.azure.account.oauth2.client.secret": client_secret,
          "fs.azure.account.oauth2.client.endpoint": f"https://login.microsoftonline.com/{tenant_id}/oauth2/token"}

    # unMount if already mounted

    if any(mount.mountPoint == f"/mnt/{storage_account_name}/{container_name}" for mount in dbutils.fs.mounts()):
        dbutils.fs.unmount(f"/mnt/{storage_account_name}/{container_name}")
    
    # Mount the storage account container
    dbutils.fs.mount(
      source = f"abfss://{container_name}@{storage_account_name}.dfs.core.windows.net/",
      mount_point = f"/mnt/{storage_account_name}/{container_name}",
      extra_configs = configs)
    
    display(dbutils.fs.mounts())




# COMMAND ----------



# COMMAND ----------

# MAGIC %md
# MAGIC ### MOUNT RAW CONTAINER

# COMMAND ----------

mount_adls('formula1dl113','bronze')

# COMMAND ----------

mount_adls('formula1dl113','silver')

# COMMAND ----------

mount_adls('formula1dl113','gold')

# COMMAND ----------

display(spark.read.csv("/mnt/formula1dl11/demo/002 circuits.csv"))

# COMMAND ----------

display(dbutils.fs.mounts())

# COMMAND ----------


