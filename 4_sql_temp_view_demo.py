# Databricks notebook source
# MAGIC %md 
# MAGIC ## Access dataframe using sql

# COMMAND ----------

# MAGIC %run ../includes/configuration

# COMMAND ----------

race_results_df = spark.read.parquet(f"{presentation_folder_path}/race_results")

# COMMAND ----------

race_results_df.createOrReplaceTempView("v_race_reasults")

# COMMAND ----------

# MAGIC %sql
# MAGIC select count(*) from v_race_reasults
# MAGIC where race_year = 2020

# COMMAND ----------

# MAGIC %sql
# MAGIC show databases

# COMMAND ----------


