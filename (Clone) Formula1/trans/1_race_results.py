# Databricks notebook source
dbutils.widgets.text("p_file_date","")
v_file_date = dbutils.widgets.get("p_file_date")

# COMMAND ----------

# MAGIC %run "../includes/configuration"

# COMMAND ----------

# MAGIC %run "../includes/common_functions"

# COMMAND ----------

drivers_df = spark.read.format("delta").load(f"{processed_folder_path}/drivers").withColumnRenamed("number","driver_number").withColumnRenamed("name","driver_name").withColumnRenamed("nationality","driver_nationality")

# COMMAND ----------

constructors_df = spark.read.format("delta").load(f"{processed_folder_path}/constructors").withColumnRenamed("name","team")

# COMMAND ----------

races_df = spark.read.format("delta").load(f"{processed_folder_path}/races").withColumnRenamed("name","race_name").withColumnRenamed("race_timestamp","race_date")

# COMMAND ----------

results_df = spark.read.format("delta").load(f"{processed_folder_path}/results").filter(f"file_date = '{v_file_date}'").withColumnRenamed("time","race_time").withColumnRenamed("race_id","results_race_id").withColumnRenamed("file_date","results_file_date")

# COMMAND ----------

circuits_df = spark.read.format("delta").load(f"{processed_folder_path}/circuits").withColumnRenamed("location","circuit_location")

# COMMAND ----------

race_cicuits_df = races_df.join(circuits_df,races_df.circuit_id==circuits_df.circuit_id).select(races_df.race_id,races_df.race_year,races_df.race_name,races_df.race_date,circuits_df.circuit_location)

# COMMAND ----------

race_results_df = results_df.join(race_cicuits_df,results_df.results_race_id==race_cicuits_df.race_id).join(drivers_df,results_df.driver_id==drivers_df.driver_Id).join(constructors_df,results_df.constructor_id==constructors_df.constructor_Id)

# COMMAND ----------

from pyspark.sql.functions import current_timestamp

# COMMAND ----------

final_df = race_results_df.select("race_id","race_year","race_name","race_date","circuit_location","driver_name","driver_number","driver_nationality","grid","fastest_lap","race_time","team","points","position","results_file_date").withColumn("created_date",current_timestamp()).withColumnRenamed("results_file_date","file_date")

# COMMAND ----------

# overwrite_partition(final_df, "f1_presentation", "race_results", "race_id")
merge_condition = "tgt.driver_name = src.driver_name AND tgt.race_id = src.race_id"

merge_delta_data(final_df,"f1_presentation","race_results",presentation_folder_path,merge_condition,"race_id")

# COMMAND ----------

# MAGIC %sql
# MAGIC select race_id,count(1) from f1_presentation.race_results 
# MAGIC group by race_id
# MAGIC order by race_id desc

# COMMAND ----------

# %sql
# -- drop table f1_presentation.race_results

# COMMAND ----------


