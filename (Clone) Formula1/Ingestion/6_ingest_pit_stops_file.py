# Databricks notebook source
dbutils.widgets.text("p_data_source","")
v_data_source = dbutils.widgets.get("p_data_source")

# COMMAND ----------

dbutils.widgets.text("p_file_date","2021-03-28")
v_file_date = dbutils.widgets.get("p_file_date")

# COMMAND ----------

# MAGIC %run "../includes/configuration"

# COMMAND ----------

# MAGIC %run "../includes/common_functions"

# COMMAND ----------

# MAGIC %md
# MAGIC ##Ingest pit_stops.json file

# COMMAND ----------

# MAGIC %md
# MAGIC ### step 1 - read json file using spark datafrsme reader API

# COMMAND ----------

from pyspark.sql.types import StructField,StructType,StringType,IntegerType

# COMMAND ----------

pit_stops_schema = StructType([StructField("raceId",IntegerType(),False),
                               StructField("driverId",IntegerType(),True),
                               StructField("stop",StringType(),True),
                               StructField("lap",IntegerType(),True),
                               StructField("time",StringType(),True),
                               StructField("duration",StringType(),True),
                               StructField("milliseconds",IntegerType(),True)
                               ])

# COMMAND ----------

pit_stops_df = spark.read.schema(pit_stops_schema).option('multiLine',True).json(f"{raw_folder_path}/{v_file_date}/pit_stops.json")

# COMMAND ----------

display(pit_stops_df)

# COMMAND ----------

# MAGIC %md
# MAGIC ### step 2 - rename column and add new column

# COMMAND ----------

from pyspark.sql.functions import current_timestamp,lit

# COMMAND ----------

final_df = pit_stops_df.withColumnRenamed("driverId","driver_id").withColumnRenamed("raceId","race_id").withColumn("ingestion_date",current_timestamp()).withColumn("data_source",lit(v_data_source)).withColumn("file_date",lit(v_file_date))

# COMMAND ----------

# MAGIC %md
# MAGIC ## step 3 write file

# COMMAND ----------

# final_df.write.mode("overwrite").format("parquet").saveAsTable("f1_processed.pit_stops")
# overwrite_partition(final_df,"f1_processed","pit_stops","race_id")


# COMMAND ----------

merge_condition = "tgt.race_id = src.race_id AND tgt.driver_id = src.driver_id AND tgt.stop =  src.stop AND tgt.race_id = src.race_id"

merge_delta_data(final_df,"f1_processed","pit_stops",processed_folder_path,merge_condition,"race_id")

# COMMAND ----------

# MAGIC %sql
# MAGIC select * from f1_processed.pit_stops

# COMMAND ----------

dbutils.notebook.exit("success")

# COMMAND ----------

# MAGIC %sql
# MAGIC select * from f1_processed.pit_stops
# MAGIC -- group by race_Id
# MAGIC -- order by race_Id desc

# COMMAND ----------

# MAGIC %sql
# MAGIC drop table f1_processed.pit_stops

# COMMAND ----------


