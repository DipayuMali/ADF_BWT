# Databricks notebook source
dbutils.widgets.text("p_data_source","")
v_data_source = dbutils.widgets.get("p_data_source")

# COMMAND ----------

dbutils.widgets.text("p_file_date","")
v_file_date = dbutils.widgets.get("p_file_date")

# COMMAND ----------

# MAGIC %run "../includes/configuration"

# COMMAND ----------

# MAGIC %run "../includes/common_functions"

# COMMAND ----------

# MAGIC %md
# MAGIC ##Ingest drivers.json

# COMMAND ----------

# MAGIC %md
# MAGIC ### step 1 - read json file using spark datafrsme reader API

# COMMAND ----------

# MAGIC %md
# MAGIC ##step2 -renamed column and add new column
# MAGIC 1.
# MAGIC 2.
# MAGIC 3.
# MAGIC 4.

# COMMAND ----------

from pyspark.sql.types import StructType,StructField,StringType,IntegerType,DateType

# COMMAND ----------

name_schema = StructType([StructField("forename",StringType(),True),
                    StructField("surname",StringType(),True)])

# COMMAND ----------

drivers_schema = StructType([StructField("driverId",IntegerType(),False),
                    StructField("driverRef",StringType(),True),
                    StructField("number",IntegerType(),True),
                    StructField("code",StringType(),True),
                    StructField("name",name_schema),
                    StructField("dob",DateType(),True),
                    StructField("nationality",StringType(),True),
                    StructField("url",StringType(),True)])

# COMMAND ----------

driver_df = spark.read.schema(drivers_schema).json(f"{raw_folder_path}/{v_file_date}/drivers.json")

# COMMAND ----------

 driver_df.printSchema()

# COMMAND ----------

display(driver_df)

# COMMAND ----------

# MAGIC %md
# MAGIC ### step 2 - rename column and add new column

# COMMAND ----------

from pyspark.sql.functions import col,concat,current_timestamp,lit

# COMMAND ----------

driver_with_column_df = driver_df.withColumnRenamed("driverId","driver_Id").withColumnRenamed("driverRef","driver_ref").withColumn("ingestion_date",current_timestamp()).withColumn("name",concat(col("name.forename"),lit(' '),col('name.surname'))).withColumn("data_source",lit(v_data_source)).withColumn("file_date",lit(v_file_date))

# COMMAND ----------

display(driver_with_column_df)

# COMMAND ----------

# MAGIC %md
# MAGIC ### step 3 - drop column

# COMMAND ----------

driver_final_df = driver_with_column_df.drop('url')

# COMMAND ----------

display(driver_final_df)

# COMMAND ----------

driver_final_df.write.mode("overwrite").format("delta").saveAsTable("f1_processed.drivers")

# COMMAND ----------

# MAGIC %fs
# MAGIC ls /mnt/formula1dl11/processed/drivers

# COMMAND ----------

# MAGIC %sql
# MAGIC select * from f1_processed.drivers

# COMMAND ----------

dbutils.notebook.exit("success")

# COMMAND ----------


